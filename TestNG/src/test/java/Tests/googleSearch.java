package Tests;


import java.net.MalformedURLException;
import java.net.URL;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.edge.EdgeOptions;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class googleSearch {


		public WebDriver driver;
		
		@Parameters("browser")
		@BeforeTest
		public void setup(String browserType) throws MalformedURLException {
			
			
			
			/*
			 * if(browserType.equalsIgnoreCase("edge")) {
			 * System.setProperty("webdriver.edge.driver", System.getProperty("user.dir")+
			 * "/src/test/resources/WebDrivers/msedgedriver.exe"); driver=new EdgeDriver();
			 * } else {
			 * System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+
			 * "/src/test/resources/WebDrivers/chromedriver.exe"); driver=new
			 * ChromeDriver(); }
			 */
			 
			 
			 
				 
			//Code for selenium grid
			
			
			
			
			  DesiredCapabilities cap = new DesiredCapabilities();
			  if(browserType.equalsIgnoreCase("chrome")) { cap.setBrowserName("chrome");
			  System.out.println("Test execution started on Chrome"); 
			  } else
			  if(browserType.equalsIgnoreCase("firefox")) { cap.setBrowserName("firefox");
			  System.out.println("Test execution started on Firefox"); } else
			  if(browserType.equalsIgnoreCase("edge")) {
			  cap.setBrowserName("MicrosoftEdge");
			  System.out.println("Test execution started on Edge"); 
			  }
			 
			
			driver = new RemoteWebDriver(new URL("http://13.234.118.190:4444/wd/hub"), cap);
			
			//driver = new RemoteWebDriver(new URL("http://localhost:4444"), cap);
			  
			driver.manage().window().maximize();
			  
			 	 
			
			
		}
		
		@Test
		public void googleOnChromeTest() {
			
				driver.get("https://www.google.com");
				WebElement serachBox= driver.findElement(By.xpath("//input[@title='Search']"));
				serachBox.clear();
				serachBox.sendKeys("Impetus");
				System.out.println("Search text entered on chrome");
				serachBox.sendKeys(Keys.ENTER);
				System.out.println("Pressed enter on chrome");
				
				   
				String expectedText="Impetus Technologies";
				 String searchText=driver.findElement(By.xpath("//h2/span[text()='Impetus Technologies']")).getText();
				    if(expectedText.equals(searchText)) {
				    	System.out.println("Search text matched successfully in chrome");
				    	Assert.assertTrue(true);
				    }
				    else {
				    	System.out.println("Search text Mismatch in chrome");
				    	Assert.assertTrue(false);
				    }
			
		}
		
		@Test
		public void googleOnFirefoxTest() {
			
			driver.get("https://www.guru99.com");
			String pageTitle=driver.getTitle();
			System.out.println(driver.getTitle());
			if(pageTitle.equals("Meet Guru99 � Free Training Tutorials & Video for IT Courses")) {
				System.out.println("Page title matched");
			}
			else {
				System.out.println("Page title not matched");
			}
			
		}
		
		@Test
		public void googleOnEdgeTest() throws InterruptedException {
			
				driver.get("https://www.guru99.com");
				String pageTitle=driver.getTitle();
				System.out.println(driver.getTitle());
				if(pageTitle.equals("Meet Guru99 � Free Training Tutorials & Video for IT Courses")) {
					System.out.println("Page title matched");
				}
				else {
					System.out.println("Page title not matched");
				}
			
		}
		
		
		@AfterTest
		public void teardown() {
			driver.close();
			
		}
		
		
	}
	
